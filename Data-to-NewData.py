import numpy as np
import pandas as pd

dataset = pd.read_csv("Data.csv");

# COLUNAS DESCARTADAS
dataset.drop(['MAE_FALECIDO'], axis=1, inplace=True)
dataset.drop(['PAI_FALECIDO'], axis=1, inplace=True)

# Colocando os nomes das tabelas em uma lista
ListaColunas = list()
for i in range (len(dataset.columns.values)):
    ListaColunas.append(str(dataset.columns.values[i]))

# Criando discionario de cada tabela
dicionario = {}
for i in range (len(dataset.columns.values)):
    nome = dataset.columns.values[i]
    dicionario[nome] = dataset[nome]

#==============================================================================
# Coluna SIT_MATRICULA foi utilizada completa
chave = list(dicionario.keys())[0]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
#Dicionario
# 0 = Matriculado
# 2 = Trancado
# 4 = Transferido interno
# 9 = Evasão
# 10 = Cancelado
# 11 = Transferido
# 13 = Aguardando coloção de grau
# 14 = Intercambio
# 18 = Formado
# 20 = Cancelamento compulsorio
# Evasão = 2, 4, 9, 10, 11, 20
# Não evasõa = 0, 13, 14, 18

# 0 = Não evasão, 1 = Evasão
lista = list([2, 4, 9, 10, 11, 20])
for i in lista:
    dataset.loc[dataset['SIT_MATRICULA']==i, 'SIT_MATRICULA']=1

lista = list([0, 13, 14, 18])
for i in lista:
    dataset.loc[dataset['SIT_MATRICULA']==i, 'SIT_MATRICULA']=0



# Coluna RENDA_FAMILIAR
chave = list(dicionario.keys())[1]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].notna().sum()
dataset[chave].mean()
dataset[chave] = dataset[chave].fillna(dataset[chave].mean())
''' Colocado a média de salarios para preencher os nan'''

# Coluna TIPO-ECOLA_ORIGEM
chave = list(dicionario.keys())[2]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].notna().sum()
dataset[chave].mode()
dataset[chave] = dataset[chave].fillna(dataset[chave].mode()[0])
dataset[chave].replace('0', 'O', inplace=True)
'''Colocamos a moda nos valores nan'''

# Coluna ANO_CONCLUSAO_2_GRAU
chave = list(dicionario.keys())[3]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].notna().sum()
int(dataset[chave].mean())
dataset[chave] = dataset[chave].fillna(int(dataset[chave].mean()))
'''Colocamos a media em formato INT nos valores nan'''

# Coluna RENDA_PER_CAPITA
chave = list(dicionario.keys())[4]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].notna().sum()
int(dataset[chave].mean())
dataset[chave] = dataset[chave].fillna(int(dataset[chave].mean()))
dataset[chave] = dataset[chave].replace(0 , dataset[chave].mode()[0])
'''Colocamos a media em formato INT nos valores nan e trocamos os valores 0 pela Moda'''

# Coluna COD_ESTADO_CIVIL
chave = list(dicionario.keys())[5]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].notna().sum()
dataset[chave].mode()
dataset[chave] = dataset[chave].fillna(dataset[chave].mode()[0])
'''Colocamos a moda nos valores nan'''

# Coluna N_FILHOS
chave = list(dicionario.keys())[6]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].notna().sum()
int(dataset[chave].mean())
dataset[chave] = dataset[chave].fillna(int(dataset[chave].mean()))
'''Colocamos a media em formato INT nos valores nan'''

# Coluna SEXO foi utilizada completa
chave = list(dicionario.keys())[7]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].notna().sum()

# Coluna PROFISSAO
chave = list(dicionario.keys())[8]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].notna().sum()
dataset[chave].mode()
dataset[chave] = dataset[chave].fillna(dataset[chave].mode()[0])
'''Colocamos a moda nos valores nan'''

# Coluna DESC_CIDADE
chave = list(dicionario.keys())[9]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].replace('Goiania', 'Goiânia', inplace=True)
'''Trocamos o nome Goiania por Goiânia buscando padronização'''

# Coluna DESC_ESTADO
chave = list(dicionario.keys())[10]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].mode()[0]
dataset[chave].replace('--', dataset[chave].mode()[0], inplace=True)
'''Colocamos a moda nos valores em '--' '''

# Coluna DESC_IDIOMA
chave = list(dicionario.keys())[11]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].notna().sum()
dataset[chave].mode()[0]
dataset[chave] = dataset[chave].fillna(dataset[chave].mode()[0])
'''Colocamos a moda nos valores nan'''

# Coluna NIVEL_FALA
chave = list(dicionario.keys())[12]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].notna().sum()
int(dataset[chave].mean())
dataset[chave] = dataset[chave].fillna(int(dataset[chave].mean()))
'''Colocamos a media em formato INT nos valores nan'''

# Coluna NIVEL_COMPREENSAO
chave = list(dicionario.keys())[13]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].notna().sum()
int(dataset[chave].mean())
dataset[chave] = dataset[chave].fillna(int(dataset[chave].mean()))
'''Colocamos a media em formato INT nos valores nan'''

# Coluna NIVEL_ESCRITA
chave = list(dicionario.keys())[14]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].notna().sum()
int(dataset[chave].mean())
dataset[chave] = dataset[chave].fillna(int(dataset[chave].mean()))
'''Colocamos a media em formato INT nos valores nan'''

# Coluna NIVEL_LEITURA
chave = list(dicionario.keys())[15]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].notna().sum()
int(dataset[chave].mean())
dataset[chave] = dataset[chave].fillna(int(dataset[chave].mean()))
'''Colocamos a media em formato INT nos valores nan'''

# Coluna NOTAS
chave = list(dicionario.keys())[16]
aux = dicionario[chave]
pd.unique(dataset[chave])
dataset[chave].notna().sum()
dataset[chave].replace('06/10/2016', np.nan, inplace=True)
dataset[chave].replace('09/10/2016', np.nan, inplace=True)
dataset[chave].replace('08/10/2016', np.nan, inplace=True)
dataset[chave].replace('07/10/2016', np.nan, inplace=True)
dataset[chave].replace('05/10/2016', np.nan, inplace=True)
dataset[chave].replace('01/10/2016', np.nan, inplace=True)
dataset[chave].replace('04/10/2016', np.nan, inplace=True)
dataset[chave].replace('03/10/2016', np.nan, inplace=True)
dataset[chave].replace('02/10/2016', np.nan, inplace=True)
dataset[chave].replace('80.00', "8.00", inplace=True)
dataset[chave].replace('60.00', "6.00", inplace=True)

aux = np.array(dataset[chave])
aux = np.reshape(aux, [aux.shape[0], 1])
lista = list()
for i in range(aux.shape[0]):
    lista = np.append(lista, float(aux[i, 0]))

dataset[chave] = lista
dataset[chave].mean()
dataset[chave] = dataset[chave].fillna(dataset[chave].mean())
'''Colocamos a media em formato INT nos valores nan'''

# Coluna N_FALTAS
chave = list(dicionario.keys())[17]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].notna().sum()
int(dataset[chave].mean())
dataset[chave] = dataset[chave].fillna(int(dataset[chave].mean()))
'''Colocamos a media em formato INT nos valores nan'''

# Coluna COD_DISCIPLINA foi utilizada completa
chave = list(dicionario.keys())[18]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])


# Coluna CARGA_HOR foi utilizada completa
chave = list(dicionario.keys())[19]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].notna().sum()

# Coluna ANO_INGRESSO foi utilizada completa
chave = list(dicionario.keys())[20]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].notna().sum()

# Coluna IDADE_INGRESSO foi utilizada completa
chave = list(dicionario.keys())[21]
aux = dicionario[chave]
aux.describe()
pd.unique(dataset[chave])
dataset[chave].notna().sum()
#==============================================================================

dataset.to_csv("NewData.csv", index= False)

