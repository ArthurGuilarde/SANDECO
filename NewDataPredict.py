import numpy as np
import pandas as pd

dataset = pd.read_csv('NewData.csv')

X = np.array(dataset.drop(['TIPO_ESCOLA_ORIGEM','COD_ESTADO_CIVIL', 'SEXO', 
                           'PROFISSAO', 'DESC_CIDADE', 'DESC_ESTADO', 
                           'DESC_IDIOMA', 'COD_DISCIPLINA'], axis=1))
y = np.array(dataset['SIT_MATRICULA'])
# 1 = Evasão, 0 = Não Evasão

# Realizar o encoding dos indices:
# 1, 4, 6, 7, 8, 9, 10



# Como recuperar
#teste = aux.dot(onehotencoder.active_features_)
#labelencoder_COD_DISCIPLINA.inverse_transform(aux)

#=============================Pré Processing===================================
# Enconding CategoricalData  TIPO_ESCOLA_ORIGEM
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_TIPO_ESCOLA_ORIGEM = LabelEncoder()
aux = labelencoder_TIPO_ESCOLA_ORIGEM.fit_transform(dataset['TIPO_ESCOLA_ORIGEM'])
aux = np.reshape(aux, [aux.shape[0],1])
onehotencoder_TIPO_ESCOLA_ORIGEM = OneHotEncoder(categorical_features = [0])
aux = onehotencoder_TIPO_ESCOLA_ORIGEM.fit_transform(aux).toarray()
X = np.append(X, aux[:, :-1], axis=1)

# Enconding CategoricalData  COD_ESTADO_CIVIL
labelencoder_COD_ESTADO_CIVIL = LabelEncoder()
aux = labelencoder_COD_ESTADO_CIVIL.fit_transform(dataset['COD_ESTADO_CIVIL'])
aux = np.reshape(aux, [aux.shape[0],1])
onehotencoder_COD_ESTADO_CIVIL = OneHotEncoder(categorical_features = [0])
aux = onehotencoder_COD_ESTADO_CIVIL.fit_transform(aux).toarray()
X = np.append(X, aux[:, :-1], axis=1)


# Enconding CategoricalData  SEXO
labelencoder_SEXO = LabelEncoder()
aux = labelencoder_SEXO.fit_transform(dataset['SEXO'])
aux = np.reshape(aux, [aux.shape[0],1])
onehotencoder_SEXO = OneHotEncoder(categorical_features = [0])
aux = onehotencoder_SEXO.fit_transform(aux).toarray()
X = np.append(X, aux[:, :-1], axis=1)

# Enconding CategoricalData  PROFISSAO
labelencoder_PROFISSAO = LabelEncoder()
aux = labelencoder_PROFISSAO.fit_transform(dataset['PROFISSAO'])
aux = np.reshape(aux, [aux.shape[0],1])
onehotencoder_PROFISSAO = OneHotEncoder(categorical_features = [0])
aux = onehotencoder_PROFISSAO.fit_transform(aux).toarray()
X = np.append(X, aux[:, :-1], axis=1)

# Enconding CategoricalData  DESC_CIDADE
labelencoder_DESC_CIDADE = LabelEncoder()
aux = labelencoder_DESC_CIDADE.fit_transform(dataset['DESC_CIDADE'])
aux = np.reshape(aux, [aux.shape[0],1])
onehotencoder_DESC_CIDADE = OneHotEncoder(categorical_features = [0])
aux = onehotencoder_DESC_CIDADE.fit_transform(aux).toarray()
X = np.append(X, aux[:, :-1], axis=1)

# Enconding CategoricalData  DESC_ESTADO
labelencoder_DESC_ESTADO = LabelEncoder()
aux = labelencoder_DESC_ESTADO.fit_transform(dataset['DESC_ESTADO'])
aux = np.reshape(aux, [aux.shape[0],1])
onehotencoder_DESC_ESTADO = OneHotEncoder(categorical_features = [0])
aux = onehotencoder_DESC_ESTADO.fit_transform(aux).toarray()
X = np.append(X, aux[:, :-1], axis=1)

# Enconding CategoricalData  DESC_IDIOMA
labelencoder_DESC_IDIOMA = LabelEncoder()
aux = labelencoder_DESC_IDIOMA.fit_transform(dataset['DESC_IDIOMA'])
aux = np.reshape(aux, [aux.shape[0],1])
onehotencoder_DESC_IDIOMA = OneHotEncoder(categorical_features = [0])
aux = onehotencoder_DESC_IDIOMA.fit_transform(aux).toarray()
X = np.append(X, aux[:, :-1], axis=1)

# Enconding CategoricalData  COD_DISCIPLINA
labelencoder_COD_DISCIPLINA = LabelEncoder()
aux = labelencoder_COD_DISCIPLINA.fit_transform(dataset['COD_DISCIPLINA'])
aux = np.reshape(aux, [aux.shape[0],1])
onehotencoder_COD_DISCIPLINA = OneHotEncoder(categorical_features = [0])
aux = onehotencoder_COD_DISCIPLINA.fit_transform(aux).toarray()
X = np.append(X, aux[:, :-1], axis=1)
 
#=============================Normalizando Data========================================
from sklearn.preprocessing import StandardScaler
stx = StandardScaler()
X = stx.fit_transform(X)

#===================================KNN========================================
from sklearn.neighbors import KNeighborsClassifier
from sklearn import model_selection 
kfold = model_selection.KFold(n_splits = 10, random_state=1)
knn = KNeighborsClassifier(n_neighbors=3)

results = model_selection.cross_val_score(knn, X, y, cv=kfold)
print("Média de Kfold 10 usando KNN: %.4f" % np.mean(results))


#===================================SVM========================================
from sklearn.svm import SVC
from sklearn import model_selection 
kfold = model_selection.KFold(n_splits = 10, random_state=1)
sv = SVC(C=2.0)

results = model_selection.cross_val_score(sv, X, y, cv=kfold)
print("Média de Kfold 10 usando SVR: %.6f" % np.mean(results))


#===================================TREE========================================
from sklearn.tree import DecisionTreeClassifier
from sklearn import model_selection 
kfold = model_selection.KFold(n_splits = 10, random_state=1)

dtc = DecisionTreeClassifier(max_features="sqrt", random_state=1)

results = model_selection.cross_val_score(dtc, X, y, cv=kfold)
print("Média de Kfold 10 usando DTR: %.6f" % np.mean(results))






